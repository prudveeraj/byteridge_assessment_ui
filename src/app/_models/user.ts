﻿export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    token: string;
}
export class Audit {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    token: string;
    loginTime: number;
    logoutTime: number;
    ip: string;
}