﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { User, Audit } from '@/_models';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`${config.apiUrl}/users`);
    }
    auditUsers() {                            // http call to get all users with role,clentI etc.
        let headers = new HttpHeaders({
            token: JSON.parse(localStorage.getItem('currentUser')).token
          });
          let options = {
            headers: headers,
          }
        return this.http.get<Audit[]>(`${config.apiUrl}/audit`,options);
    }
    register(user: User) {
        return this.http.post(`${config.apiUrl}/users/register`, user);
    }

    delete(id: number) {
        return this.http.delete(`${config.apiUrl}/users/${id}`);
    }
}