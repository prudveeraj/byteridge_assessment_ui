﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from '@/_models';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient, private router: Router) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username, password) {
        return this.http.post<any>(`${config.apiUrl}/users/authenticate`, { username, password })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            }));
    }

    logout(username, unauthorized?: any) {
        // remove user from local storage and set current user to null
        if(username){
            this.http.post<any>(`${config.apiUrl}/users/logout`, { username: username}) // updating logout time to user
            .subscribe(user => {
                localStorage.removeItem('currentUser');
                this.currentUserSubject.next(null);
                if(unauthorized) this.router.navigate(['/login'], { queryParams: { unauthorized: "yes" }});
                else this.router.navigate(['/login']);
            });
        } else {
            localStorage.removeItem('currentUser');
        }

    }
}
