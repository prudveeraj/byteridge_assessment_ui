import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { User } from '@/_models';
import { UserService, AuthenticationService } from '@/_services';
import { first } from 'rxjs/operators';
@Component({
  templateUrl: './auditor.component.html'})
export class AuditorComponent implements OnInit {        //this component serve for GET /audit page

  currentUser: User;
  users = [];
  dataLoaded: Promise<boolean>;

  constructor(
      private authenticationService: AuthenticationService,
      private userService: UserService
  ) {
      this.currentUser = this.authenticationService.currentUserValue;
  }

  dtOptions: DataTables.Settings = {};

  ngOnInit() {
    this.userService.auditUsers().subscribe((data) => {   // http call to get all users with role,clentI etc.
      this.users = data;
      for(let i=0; i<this.users.length;i++){
        if(this.users[i].login_history.length > 0){    
          this.users[i].loginTime = new Date(this.users[i].login_history[this.users[i].login_history.length - 1]);
          this.users[i].logoutTime = new Date(this.users[i].logout_history[this.users[i].logout_history.length - 1]);
        } else {                                                      
          this.users[i].loginTime = new Date(this.users[i].loginTime); //for statisfying old records who have only last login and logout time
          this.users[i].logoutTime = new Date(this.users[i].logoutTime)
         }

      }
      this.dataLoaded = Promise.resolve(true);
    }, (err) => {
        console.log('-----> err', err);
      }
    );    
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      lengthMenu: [10,20,30],
      processing: true
    };
}

}
